#include "string/sa.hpp"

int main() {
	char s[] = "banana";
	SA<10> sa(s);
	const int n = strlen(s);
	REP(i, n) {
		printf("%d %s\n", sa.height[i], s + sa.sa[i]);
	}
	return 0;
}