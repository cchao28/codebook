#include "math/mod.hpp"

int main() {
	assert(pow_mod(3, 20, 33) == 12);
	return 0;
}