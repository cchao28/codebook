#include "math/sieve.hpp"

int main() {
	const static int n = 20;
	Sieve<n> sieve;

	vector<int> prime = {2, 3, 5, 7, 11, 13, 17, 19};
	assert(equal(sieve.prime.begin(), sieve.prime.end(), prime.begin()));

	return 0;
}