#include "math/prime_test.hpp"

int main() {
	assert(isprime(29996224275833LL));
	assert(isprime(2147483647));
	assert(!isprime(15));
	assert(!isprime(6811297));
	return 0;
}