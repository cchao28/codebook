#include "ds/bit.hpp"

int main() {
	BinaryIndexedTree<int, 20> t;
	t.add(1, 1);
	t.add(2, 2);
	t.add(3, 3);
	assert(t.sum(4) == 6);
	t.add(3, 3);
	assert(t.sum(4) == 9);

	return 0;
}
