#include "ds/treap.hpp"

void traverse(Treap *x, int dep) {
	if(!x) return ;
	traverse(x->left, dep+1);
	printf("%d: size %d dep %d\n", x->value, (int)x->size, dep);
	traverse(x->right, dep+1);
}

int main() {
	srand(time(NULL));
	Treap *r = nullptr;
	REPE(i, 1, 10) {
		r = merge(r, new Treap(i));
	}
	traverse(r, 0);
	puts("--");
	auto t = split(r, 5);
	traverse(t.F, 0);
	puts("--");
	traverse(t.S, 0);

	return 0;
}