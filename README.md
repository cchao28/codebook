Codebook
===

This repository will contain some clean parts of my codebook.

## Mathematics
- Modular Arithmetics
	- Power
	- Multiply
	- Extended Euclidean Algorithm
	- Modular Inverse
	- Modular Inverse Table
	- Chinese Remainder Theorem
	- Shank's baby-step giant-step algorithm
- Determinant
- Linear Sieves
- Fast Fourier transform
- Number Theoretic Transform
- [Karatsuba algorithm](https://en.wikipedia.org/wiki/Karatsuba_algorithm)
- Miller–Rabin primality test
- Simpson's rule
- Simplex algorithm
- Gaussian elimination
- Pollard's rho algorithm
- Formula
	- Catalan number
	- Stirling number
	- Bernoulli number

## Data Structure
- [Non-Recursive Segment Tree](http://codeforces.com/blog/entry/18051)
- Treap
- Persistent Treap
- Binary Indexed Tree
- Leftist Tree
- Dynamic Tree
- Dancing Links and Algorithm X


## Geometry
- Basic Operation
- Rotating calipers
- Smallest-circle problem
- Half-Plane Intersection

## Stringology
- Aho-Corasick automation
- [Palindromic tree](http://adilet.org/blog/25-09-14/)
- Manacher's algorithm
- [Cyclic Longest Common Subsequence](http://arxiv.org/pdf/1208.0396.pdf)
- Suffix Array in O(N lg^2 N)
- Suffix Array in O(N lg N)
- Suffix Array in O(N)
- Suffix automaton
- Knuth–Morris–Pratt algorithm
- Lexicographically minimal string rotation

## Graph Theory
- 2-CNF
- Dinic's algorithm
- Minimum Cost Maximum Flow
- Heavy Light Decomposition
- Connectivity
	- Articulation Points
	- Bridges
	- Biconnected component
	- Strongly connected component
- Unweighted bipartite matching
	- Hopcroft–Karp algorithm
- Weighted bipartite matching
- Blossom algorithm
- Maximum Clique
- Isomorphism
- Zhu-Liu Algorithm
- Stoer–Wagner algorithm
- Formula

## Miscellaneous
- Count Inversions
- [Largest Rectangle in Histogram](https://leetcode.com/problems/largest-rectangle-in-histogram/)
- Multiple knapsack problem
- Manhattan distance minimum spanning tree
- Maximum average intervals of length > k (deque DP)

## Programming Language
- C++ Tricks
- Java Cheatsheet
