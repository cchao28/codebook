#!/bin/bash

function test {
	g++ -o tmp.out -std=c++14 -Isrc $1 -ggdb
	./tmp.out
}

# test test/ds/BIT.cpp
# test test/ds/Treap.cpp
# test test/math/sieve.cpp
# test test/math/mod.cpp
# test test/math/prime_test.cpp
test test/string/sa.cpp