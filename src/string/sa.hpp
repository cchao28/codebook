#include "common.h"

template<size_t maxn>
struct SA {
	const char *s;
	const int n;
	int sa[maxn], height[maxn];
	int pos[maxn], tmp[maxn], rank[maxn];
	SA(const char *s) : s(s), n(strlen(s)) {
		build_sa();
		build_height();
	}
	void build_sa() {
		REP(i, n) {
			sa[i] = i;
			pos[i] = s[i];
		}
		for(int gap = 1; ; gap += gap) {
			auto cmp = [&](int i, int j) {
				if(pos[i] != pos[j]) return pos[i] < pos[j];
				i += gap; j += gap;
				return (i < n && j < n) ? pos[i] < pos[j] : i > j;
			};
			sort(sa, sa + n, cmp);
			tmp[0] = 0;
			REP(i, n - 1) tmp[i + 1] = tmp[i] + cmp(sa[i], sa[i + 1]);
			REP(i, n) pos[sa[i]] = tmp[i];
			if(tmp[n - 1] == n - 1) break;
		}
	}
	void build_height() {
		for(int i = 0; i < n; i++) rank[sa[i]] = i;
		height[0] = 0;
		for(int i = 0, k = 0; i < n; i++) {
			if(rank[i] == 0) continue;
			if(k) k--;
			int j = sa[rank[i]-1];
			while(s[i+k] == s[j+k]) k++;
			height[rank[i]] = k;
		}
	}
};
