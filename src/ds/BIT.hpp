#include "common.h"

template<typename T, size_t N>
struct BinaryIndexedTree {
	T a[N + 1];
	BinaryIndexedTree() {
		fill(a, a + N + 1, 0);
	}
	inline size_t lowBit(size_t x) {
		return x & -x;
	}
	T sum(size_t p) {
		T ret = 0;
		for(; p > 0; p -= lowBit(p))
			ret += a[p];
		return ret;
	}
	void add(size_t p, T x) {
		for(; p <= N; p += lowBit(p))
			a[p] += x;
	}
};
