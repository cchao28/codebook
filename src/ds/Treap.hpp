#include "common.h"

struct Treap {
	int value;
	size_t size;
	int priority;
	Treap *left, *right;
	Treap(int value = 0): value(value), size(1), priority(rand()), left(NULL), right(NULL) { }
};

int size(Treap *x) {
	return x ? x-> size : 0;
}

void maintain(Treap *x) {
	if(x) {
		x->size = 1 + size(x->left) + size(x->right);
	}
}

pair<Treap*, Treap*> split(Treap *t, size_t k) {
	if(!t or size(t) <= k)
		return {t, nullptr};
	else if(k <= 0)
		return {nullptr, t};

	pair<Treap*, Treap*> ret;
	if(size(t->left) >= k) {
		ret.S = t;
		tie(ret.F, t->left) = split(t->left, k);
	}
	else {
		ret.F = t;
		tie(t->right, ret.S) = split(t->right, k - 1 - size(t->left));
	}
	maintain(ret.F);
	maintain(ret.S);
	return ret;
}

Treap* merge(Treap *left, Treap *right) {
	if(!left or !right) {
		return !left ? right : left;
	}
	if(left->priority > right->priority) {
		left->right = merge(left->right, right);
		maintain(left);
		return left;
	}
	else {
		right->left = merge(left, right->left);
		maintain(right);
		return right;
	}
}
