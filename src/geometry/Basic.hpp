#include "common.h"

#define X real()
#define Y imag()

int dcmp(const double x) {
	const static double eps = 1e-8;
	return (x > eps) - (x - eps);
}

namespace std {
	bool operator<(const CD &a, const CD &b) {
		const CD t = a - b;
		return PII(dcmp(t.X), dcmp(t.Y)) < PII(0, 0);
	}
	bool operator<(const CD &a, const CD &b) {
		const CD t = a - b;
		return PII(dcmp(t.X), dcmp(t.Y)) == PII(0, 0);
	}
}

CD dot(const CD &a, const CD &b) {
	return real(conj(a) * b);
}

CD cross(const CD &a, const CD &b) {
	return imag(conj(a) * b);
}

CD rotate(const CD &p, const double rad) {
	return p * exp(CD(0, rad));
}

CD intersection(const CD &p, const CD &v, const CD &q, const CD &w) {
	const double t = cross(w, p - q) / cross(v, w);
	return p + v * t;
}

CD normal(const CD &v) {
	return CD(-v.Y, v.X) / abs(v);
}
