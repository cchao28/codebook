// Modular Arithmetic

#include "common.h"

LL mul_mod(LL a, LL p, const LL m) {
	LL ret = 0;
	for(; p; p >>= 1, a = (a + a) % m)
		if(p&1) ret = (ret + a) % m;
	return ret;
}

LL pow_mod(LL a, LL p, const LL m) {
	LL ret = 1;
	for(; p; p >>= 1, a = mul_mod(a, a, m))
		if(p&1) ret = mul_mod(ret, a, m);
	return ret % m;
}

void ext_gcd(LL a, LL b, LL &d, LL &x, LL &y) {
	if(!b) { d = a, x = 1, y = 0; }
	else { ext_gcd(b, a % b, d, y, x); y -= x * (a / b); }
}

void inv_table(int n, const int mod, int *inv_t) {
	inv_t[1] = 1;
	for(int i = 2; i < n; ++i)
		inv_t[i] = (mod - 1LL * (mod / i) * inv_t[mod % i] % mod) % mod;
}

LL inv(LL a, LL n) {
	LL d, x, y;
	ext_gcd(a, n, d, x, y);
	return d == 1 ? (x % n + n) % n : -1;
}

//return x, x % m[i] = a[i]
LL crt(int n, LL *a, LL *m) {
	LL M = 1, d, x = 0, y;
	for(int i = 0; i < n; ++i) M *= m[i];
	for(int i = 0; i < n; ++i) {
		LL w = M / m[i];
		ext_gcd(m[i], w, d, d, y);
		x = (x + y * w * a[i]) % M;
	}
	return (x + M) % M;
}
//return x, (a ** x) % n= b
int log_mod(int a, int b, int n) {
	int m, v, e = 1;
	m = ceil(sqrt(n + 0.5));
	v = inv(pow_mod(a, m, n), n);
	unordered_map<int, int> x;
	x[1] = 0;
	for(int i = 1; i < m; ++i) {
		e = mul_mod(e, a, n);
		if(!x.count(e)) x[e] = i;
	}
	for(int i = 0; i < m; ++i) {
		if(x.count(b)) return i * m + x[b];
		b = mul_mod(b, v, n);
	}
	return -1;
}
