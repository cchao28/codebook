#include "common.h"

template<size_t n>
struct Sieve {
	bitset<n> vis;
	vector<int> prime;
	int mu[n], phi[n];
	Sieve() {
		MS0(mu);
		MS0(phi);
		vis.reset();
		mu[1] = phi[1] = 1;
		for(int i = 2; i < n; ++i) {
			if(!vis.test(i)) {
				prime.PB(i);
				mu[i] = -1;
				phi[i] = i - 1;
			}
			for(auto x : prime) {
				if(1LL * i * x >= n)
					break;
				int t = i * x;
				vis.set(t);
				if(i % x == 0) {
					mu[t] = 0;
					phi[t] = phi[i] * x;
					break;
				}
				else {
					mu[t] = -mu[i];
					phi[t] = phi[i] * (x - 1);
				}
			}
		}
	}
};